import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AiqDsAngularModule } from '@aiq-ds/angular/dist';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AiqDsAngularModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
